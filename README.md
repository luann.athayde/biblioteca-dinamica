# biblioteca-dinamica

`Versão atual: 1.0.0`
`Autor: Luann Athayde`

Carregador dinamico de bibliotecas.

Biblioteca utilizada para carregar dinamicamente modulos(DLL ou SO) 
para a JVM mesmo estando dentro de arquivos JAR/WAR.

**Uso:**

Metódo principal: _BibliotecaDinamica.carregarBiblioteca_

**Parametros possíveis:**

**nome:string** = nome da biblioteca que está na pasta de recursos

**md5:string** = md5sum da biblioteca para checar a integridado 
(valor null ignora checagem)

**apagarArquivoTemporario:boolean** = idicador utilizado para apagar arquivo
temporario apos carregar a biblioteca

**recarregar:boolean** = força o recarregamento da biblioteca mesmo depois de já
ter sido carregada.

---
###### Quantum Software &copy; 2019 - Luann Athayde