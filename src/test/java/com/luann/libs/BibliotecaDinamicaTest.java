package com.luann.libs;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BibliotecaDinamicaTest {

    private String nomeBiblioteca;
    private String md5Biblioteca;

    @Before
    public void setup() {
        nomeBiblioteca = "OpenAL.dll";
        md5Biblioteca = "0b6079feaa433a0a5b74cb2d4221ee96";
    }

    @Test
    public void carregarBibliotecaTest() {

        boolean carregado = BibliotecaDinamica.carregarBiblioteca(
                nomeBiblioteca,
                md5Biblioteca
        );
        Assert.assertEquals(carregado, true);
        Assert.assertEquals(
                BibliotecaDinamica.getBibliotecasCarregadas().containsKey(nomeBiblioteca),
                true
        );

        carregado = BibliotecaDinamica.carregarBiblioteca(
                nomeBiblioteca,
                md5Biblioteca
        );
        Assert.assertEquals(carregado, true);
        Assert.assertEquals(
                BibliotecaDinamica.getBibliotecasCarregadas().containsKey(nomeBiblioteca),
                true
        );

        carregado = BibliotecaDinamica.carregarBiblioteca(
                nomeBiblioteca,
                md5Biblioteca,
                true,
                true
        );
        Assert.assertEquals(carregado, true);
        Assert.assertEquals(
                BibliotecaDinamica.getBibliotecasCarregadas().containsKey(nomeBiblioteca),
                true
        );

        carregado = BibliotecaDinamica.carregarBiblioteca(
                nomeBiblioteca,
                md5Biblioteca,
                false,
                true
        );
        Assert.assertEquals(carregado, true);
        Assert.assertEquals(
                BibliotecaDinamica.getBibliotecasCarregadas().containsKey(nomeBiblioteca),
                true
        );

        // ---
        // MD5 errado
        carregado = BibliotecaDinamica.carregarBiblioteca(
                nomeBiblioteca,
                md5Biblioteca+"123",
                false,
                true
        );
        Assert.assertEquals(carregado, false);

    }

}
