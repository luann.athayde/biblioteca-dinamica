package com.luann.libs;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * @author Luann Athayde
 * @version 1.0
 * @sice 17/05/2019
 */
public class BibliotecaDinamica {

    /* Somente metodos estaticos */
    private BibliotecaDinamica() {
    }

    private static final Map<String, Boolean> bibliotecasCarregadas = new HashMap<>();
    private static final java.util.logging.Logger log = java.util.logging.Logger.getLogger("libraryLoader");

    public static boolean carregarBiblioteca(String nome, String md5) {
        return carregarBiblioteca(nome, md5, false, false);
    }

    public static boolean carregarBiblioteca(String nome, String md5, boolean apagarArquivoTemporario, boolean recarregar) {
        try {
            log.log(Level.INFO, "Carregando biblioteca [" + nome + "]...");
            if (!recarregar && bibliotecasCarregadas.containsKey(nome)) {
                log.log(Level.INFO, "Biblioteca [" + nome + "] já está carregada...");
                return bibliotecasCarregadas.get(nome);
            }
            // ---
            BufferedInputStream streamBiblioteca = new BufferedInputStream(
                    ClassLoader.getSystemResourceAsStream(nome),
                    1024 // 1KB
            );
            // ---
            int tamanhoBiblioteca = streamBiblioteca.available();
            byte dados[] = new byte[tamanhoBiblioteca];
            int dadosCarregados = streamBiblioteca.read(dados, 0, tamanhoBiblioteca);
            streamBiblioteca.close();
            if (dadosCarregados != tamanhoBiblioteca) {
                log.log(Level.SEVERE, "Falha ao carregar biblioteca! Tamanho: " + tamanhoBiblioteca + " | Total carregado: " + dadosCarregados);
                return false;
            }

            String hash = calcularHash(dados, "MD5");
            if (md5 != null && !md5.isEmpty() && !hash.equals(md5)) {
                log.log(Level.SEVERE, "Arquivo de biblioteca corrompido!");
                return false;
            }

            String prefixoAplicacao = System.getProperty("appName");
            if (prefixoAplicacao == null || prefixoAplicacao.isEmpty()) {
                prefixoAplicacao = "app";
            }

            File bibliotecaTemporaria = File.createTempFile(prefixoAplicacao + "_", "_" + nome);
            bibliotecaTemporaria.deleteOnExit();
            Files.copy(
                    new ByteArrayInputStream(dados),
                    bibliotecaTemporaria.getAbsoluteFile().toPath(),
                    java.nio.file.StandardCopyOption.REPLACE_EXISTING
            );

            String caminhoBiblioteca = bibliotecaTemporaria.getAbsolutePath();
            log.log(Level.INFO, "Biblioteca temporaria [" + nome + "]: " + caminhoBiblioteca);
            System.load(caminhoBiblioteca);

            log.log(Level.INFO, "Biblioteca [" + nome + "] carregada...");
            if (apagarArquivoTemporario) {
                log.log(Level.INFO, "Arquivo temporario apagado: " + bibliotecaTemporaria.delete());
            }
            bibliotecasCarregadas.put(nome, true);
        } catch (Exception e) {
            bibliotecasCarregadas.put(nome, true);
            log.log(Level.SEVERE, "Falha ao carregar biblioteca dinamica [" + nome + "]!", e);
            return false;
        }
        return true;
    }

    public static String calcularHash(byte[] dados, String algoritimo) {
        try {
            MessageDigest md = MessageDigest.getInstance(algoritimo);
            md.update(dados);
            byte[] md5 = md.digest();
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < md5.length; i++) {
                int highPart = ((md5[i] >> 4) & 0xf) << 4;
                int lowPart = md5[i] & 0xf;
                if (highPart == 0) {
                    s.append('0');
                }
                s.append(Integer.toHexString(highPart | lowPart));
            }
            return s.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Map<String, Boolean> getBibliotecasCarregadas() {
        Map<String, Boolean> copia = new HashMap<>();
        for (String key : bibliotecasCarregadas.keySet()) {
            copia.put(key, bibliotecasCarregadas.get(key));
        }
        return copia;
    }

}
